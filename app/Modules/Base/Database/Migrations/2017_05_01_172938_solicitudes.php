<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Solicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function(Blueprint $table){
            $table->increments('id');
            $table->integer('tipo_solicitud')->unsigned();
            $table->integer('solicitante')->unsigned();
            $table->integer('aquien')->unsigned();
            $table->integer('indece')->unsigned();

            $table->timestamps();
        });
    }
    /*
    Tipo solicitud 
        1 = contrato nuevo
        2 = renovacion contrato
        3 = agregar beneficiarios
        4 = modificacion de informacion personal

    Solicitante 
        id_persona quien solicita.
    aquien 
        1-contrato
        2- perona
    
    indice 
        persona_id o contrato_id
    */
        
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
