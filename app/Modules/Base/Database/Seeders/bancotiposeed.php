<?php

namespace App\Modules\Base\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Modules\Base\Models\BancoTipoCuenta;
use DB;
class bancotiposeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        DB::beginTransaction();
        try{
            BancoTipoCuenta::create([
                'nombre' => 'Ahorro'
            ]);
            BancoTipoCuenta::create([
                'nombre' => 'Corriente'
            ]);
            BancoTipoCuenta::create([
                'nombre' => 'Targeta de Credito'
            ]);
        }catch(Exception $e){
            DB::rollback();
            echo "Error ";
        }
        DB::commit();
    }
}
