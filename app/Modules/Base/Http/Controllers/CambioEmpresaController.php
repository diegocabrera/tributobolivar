<?php

namespace App\Modules\Base\Http\Controllers;
use Session;
use App\Modules\Base\Http\Controllers\Controller;
use App\Http\Requests\Request;
use Auth;

class CambioEmpresaController extends Controller {
	public $autenticar = false;

	public function __construct() {
		parent::__construct();
	}

	public function cambio(Request $request){

		$empresa = $request->id;
		Session::put(['empresa' => $empresa]);
		return ['s'=>'s'];
	}	
}