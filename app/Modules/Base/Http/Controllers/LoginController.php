<?php

namespace App\Modules\Base\Http\Controllers;

//Dependencias
use Illuminate\Support\Facades\Auth;
use Session;

//db
use DB;
use Mail;

//Request
use Illuminate\Http\Request;
use App\Modules\Base\Http\Requests\LoginRequest;
use App\Modules\Base\Http\Requests\LoginfotoRequest;
use App\Modules\Base\Http\Requests\RegistroRequest;

//Controlador Padre
use App\Modules\Base\Http\Controllers\Controller;

//Modelos
use App\Modules\Base\Models\Usuario;

use App\Modules\Empresa\Models\Empresa;
use App\Modules\Base\Models\Personas;
use App\Modules\Base\Models\PersonasDetalles;
use App\Modules\Base\Models\TipoPersona;
use App\Modules\Base\Models\PersonasDireccion;
use App\Modules\Base\Models\AppUsuarioEmpresa;
use App\Modules\Base\Models\PreguntaSeguridad;
use App\Modules\Base\Models\PersonasCorreo;
use App\Modules\Base\Models\PersonasTelefono;
use App\Modules\Base\Models\RegistroUsuario;
use App\Modules\Base\Models\RespuestaPregunta;
use App\Modules\Base\Models\Estados;
use App\Modules\Base\Models\Municipio;
use App\Modules\Base\Models\Parroquia;
use App\Modules\Base\Models\Ciudades;
//MODELO PARA RECUPERAR CONTRASEÑA
use App\Modules\Base\Models\RecuperarPass;

class LoginController extends Controller {

	public $autenticar = false;

	protected $redirectTo = '/';

	protected $redirectPath = '/';

	protected $prefijo = '';

	public $librerias = [
        'jquery-ui',
        'jquery-ui-timepicker',
        'bootstrap-sweetalert',
        'bootstrap-wizard'
	];
	public $js = [
        'moment.min.js',
        'jquery.validate.min.js',
        'additional-methods.min.js',
        'bootstrap-wizard/jquery.bootstrap.wizard.min.js',
        'select2.full.min.js'
    ];

	public function __construct() {
		//$this->middleware('guest', ['except' => 'getSalir']);
		$this->prefijo = \Config::get('admin.prefix');

		$this->redirectTo = $this->prefijo . $this->redirectTo;
		$this->redirectPath = $this->prefijo . $this->redirectPath;

		if (Auth::check()) {
			return redirect($this->prefijo . '/');
		}
	}

	public function index() {
		if (Auth::check()) {
			return redirect($this->prefijo . '/');
		}
		$empresas = Empresa::all()->pluck('nombre', 'id');
		$tipo = TipoPersona::all()->pluck('nombre', 'id');
		$preguntas = PreguntaSeguridad::all()->pluck('nombre', 'id');

		return $this->view('base::Login', [
			'tipo' => $tipo,
			'Personas_direccion' => new PersonasDireccion(),
			'preguntas' => $preguntas
		]);
	}

	public function bloquear() {
		return $this->view('base::Bloquear');
	}

	public function salir() {
		Auth::logout();
		return redirect($this->prefijo . '/login');
	}

	public function validar(LoginRequest $request) {
		$data = $request->only('usuario', 'password');
		$data['usuario'] = strtolower($data['usuario']);
		$autenticado = Auth::attempt($data, $request->recordar());
		$user = Usuario::where('usuario', $data['usuario'])->first();
		if($user && $user->verificado == 'n'){
			return ['s' => 'n', 'msj' => 'Hemos enviado un mensaje de confirmación a su correo, por favor ingrese y verifique su cuenta.'];
		}
		$idregistro = '';
		$login = $data['usuario'];

		if (!$autenticado) {
			$idregistro = 'Clave:' . $data['password'];
			$data = [
				'usuario' => $data['usuario'],
				'password' => $data['password']
			];

			$autenticado = Auth::attempt($data, $request->recordar());
		}

		if ($autenticado) {

			if(Auth::user()->super == 'n'){
				//OMAR ESTA SESION ES PARA VALIDAR SI EL USUARIO TIENE PERMISO A LA EMPRESA
				$permisos = AppUsuarioEmpresa::where('usuario_id',Auth::user()->id)
				 			->where('empresa_id', $request->empresa)->get();
				if($permisos->count() == 0 ){
				 	Auth::logout();
				 	return ['s' => 'n', 'msj' => 'Error de acceso denegado'];
				}

				$permis = AppUsuarioEmpresa::select('empresa_id')->where('usuario_id',Auth::user()->id)->get();
				$datos = [];
				foreach ($permis as $permi) {
					$datos[]= $permi->empresa_id;
				}

				Session::put(['empresas_permisos' =>$datos]);
				Session::put(['empresa' => 5]);

			}else{
				Session::put(['empresa' => 5]); //si es super user
			}
			return ['s' => 's'];
		}
		return ['s' => 'n', 'msj' => 'La combinacion de Usuario y Clave no Concuerdan.'];
	}

	public function foto(LoginfotoRequest $request){

		$usuario = $request->usuario;

		$user = Usuario::select('personas_id')->where('usuario','=', $usuario)->first();
			$foto = '';
		if($user){
			$_foto = Personas::select('foto')->where('id','=',$user->personas_id)->first();
			$foto= $_foto->foto;
		}

		return ['foto'=>$foto];
	}

	protected function random_string($length = 10)
	{
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
	}//omar

	public function registro(Request $request, $id= 0)
	{
		DB::beginTransaction();
		try {
			$data = $request->all();
			$persona = Personas::create([
				"tipo_persona_id" => $request->tipo_persona,
				"dni"             => $request->dni,
				"nombres"         => $request->nombres,
				"foto"            => 'usuario.png'
			]);

			$data['personas_id'] = $persona->id;

			$code_verificacion = $this->random_string(20);

			//dd($data);

			$data['perfil_id'] = 8;
			$data['usuario'] = $data['correo_pricipal'];
			$data['codigo'] = $code_verificacion;
			$data['validado'] = 'n';

			PersonasDireccion::create(
				[
					'personas_id' 	 =>  $data['personas_id'],
					"estados_id"     => $request->estados_id,
					"ciudades_id"    => $request->ciudades_id,
					"municipios_id"  => $request->municipios_id,
					"parroquias_id"  => $request->parroquias_id,
					"direccion"      => $request->direccion
				]
			);
			//telefonos
			if($request->telefono_movil != ''){
				PersonasTelefono::create([
					"personas_id" => $data['personas_id'],
					"tipo_telefono_id" => 1,
					"numero"           => $request->telefono_movil,
					"principal" => true
				]);
			}

			if($request->telefono_casa != ''){
				PersonasTelefono::create([
					"personas_id" => $data['personas_id'],
					"tipo_telefono_id" => 2,
					"numero"           => $request->telefono_casa,
					"principal" => true
				]);
			}

			if($request->telefono_oficina != ''){
				PersonasTelefono::create([
					"personas_id" => $data['personas_id'],
					"tipo_telefono_id" =>3,
					"numero"           => $request->telefono_oficina,
					"principal" => true
				]);
			}

			//correo
			PersonasCorreo::create([
				"personas_id" => $data['personas_id'],
				"correo" => $data['correo_pricipal'],
				"principal" => true
			]);
			PersonasCorreo::create([
				"personas_id" => $data['personas_id'],
				"correo" 	  => $data['correo_secundario'],
				"principal"   => false
			]);

			$usuario = Usuario::create($data);
			$id = $usuario->id;
			//preguntas respuestas de seguridad

			RespuestaPregunta::create([
				"usuario_id" 			=> $id,
				"pregunta_seguridad_id" => $data['Pregunta_1'],
				"respuesta"				=> $data['Respuesta_1']
			]);

			RespuestaPregunta::create([
				"usuario_id" 			=> $id,
				"pregunta_seguridad_id" => $data['Pregunta_2'],
				"respuesta"				=> $data['Respuesta_2']
			]);

			RespuestaPregunta::create([
				"usuario_id" 			=> $id,
				"pregunta_seguridad_id" => $data['Pregunta_3'],
				"respuesta"				=> $data['Respuesta_3']
			]);

			// Mail::send('base::emails.confirmacion', $data, function($message) use ($data) {
	    //     $message->to($data['correo_principal'], $data['nombres'])->subject('Por favor confirma tu correo');
	    // });

			Mail::send("base::emails.confirmacion", [
				'data' 						=> $data,
				'confirmacion' 		=> $code_verificacion
			], function($message) use ($data) {
			 	$message->from('tributosbolivar8001@gmail.com', 'Confirmación de Correo Electronico');
			 	$message->to($data['correo_pricipal'],$data['nombres'])->subject('Por favor confirma tu correo');
		 	});

		} catch (Exception $e) {
			DB::rollback();
			return $e->errorInfo[2];
		}

		DB::commit();

		return [

			//'id' => $usuario->id,
			//'texto' => $usuario->nombre,
			  's' => 's',

			'id' => $usuario->id,
			'texto' => $usuario->nombre,
			's' => 's',

			'msj' => 'Hemos enviado un mensaje de confirmación a su correo, por favor ingrese y verifique su cuenta.'
		];
		//return ['s' => 'n', 'msj' => 'La combinacion de Usuario y Clave no Concuerdan.'];
	}

	 public function ciudades(Request $request){
        $sql = Ciudades::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene ciudades'];

        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Ciudades encontrados', 'ciudades_id'=> $sql];
        }

        return $salida;
    }

    public function municipios(Request $request){
        $sql = Municipio::where('estados_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el estado no Contiene municipios'];

        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Municipios encontrados', 'municipios_id'=> $sql];
        }

        return $salida;
    }
    public function parroquias(Request $request){
        $sql = Parroquia::where('municipios_id', $request->id)
                    ->pluck('nombre','id')
                    ->toArray();

        $salida = ['s' => 'n' , 'msj'=> 'el municipio no Contiene parroquias'];

        if($sql){
            $salida = ['s' => 's' , 'msj'=> 'Paroquias encontrados', 'parroquias_id'=> $sql];
        }

        return $salida;
    }

	public function confirmacion($code)
	{
	        DB::beginTransaction();

	        try {
	            if ($code === 'n') {
	                return redirect($this->prefijo . '/login');
	            }

	            $usuario = Usuario::where('codigo', $code)->first();

	            if(!$usuario || $usuario->verificado == 's'){
	                return redirect($this->prefijo . '/login');
	            }

	            Usuario::find($usuario->id)->update([
	                'validado' => 's',
	            ]);

	            Mail::send("base::emails.bienvenido", [
								'usuario' => $usuario
								], function($message) use($usuario) {
								$message->from('tributosbolivar8001@gmail.com', 'Bienvenido al Sistema de Tributos Bolívar');
								$message->to($usuario->usuario, $usuario->personas(['nombres']))->subject("Bienvenido a Tributos Bolívar");
	            });


	        } catch (Exception $e) {
	            DB::rollback();
	            return $e->errorInfo[2];
	        }

	        DB::commit();

	        return redirect($this->prefijo . '/login');
	    }

	public function recuperarEnviar(Request $request){

		//$data = $request->all();
		//dd($data);
		$tokenRecuperacion = $this->random_string(20);
		// $correo = PersonasCorreo::where('correo', $request->correo_recuperar)->first();
		// //dd($request->all());
		Mail::send("base::emails.recovery",[
			'token' => $tokenRecuperacion,
		], function($message) use ($request){
						$message->from('tributosbolivar8001@gmail.com', 'Recuperación de Contraseña');
						$message->to($request->correo_recuperar)->subject("Recuperación de Contraseña");
		});

		RecuperarPass::create([
			'token' => $tokenRecuperacion,
			'email' => $request->correo_recuperar
		]);

	}

	public function recuperarContraseña ($token)
	{
		$recuperar = RecuperarPass::where('token', $token)->first();
		$correo = PersonasCorreo::where('correo', $recuperar->email)->first();
		$usuario = Usuario::where('personas_id', $correo->personas_id)->first();

		dd($usuario);
	}
}
