<?php

namespace App\Modules\Base\Http\Controllers;

//Controlador Padre
use App\Modules\Base\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Base\Http\Requests\PreguntaSeguridadRequest;

//Modelos
use App\Modules\Base\Models\PreguntaSeguridad;

class PreguntaSeguridadController extends Controller
{
    protected $titulo = 'Pregunta Seguridad';

    public $js = [
        'PreguntaSeguridad'
    ];
    
    public $css = [
        'PreguntaSeguridad'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('base::PreguntaSeguridad', [
            'PreguntaSeguridad' => new PreguntaSeguridad()
        ]);
    }

    public function nuevo()
    {
        $PreguntaSeguridad = new PreguntaSeguridad();
        return $this->view('base::PreguntaSeguridad', [
            'layouts' => 'base::layouts.popup',
            'PreguntaSeguridad' => $PreguntaSeguridad
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $PreguntaSeguridad = PreguntaSeguridad::find($id);
        return $this->view('base::PreguntaSeguridad', [
            'layouts' => 'base::layouts.popup',
            'PreguntaSeguridad' => $PreguntaSeguridad
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $PreguntaSeguridad = PreguntaSeguridad::withTrashed()->find($id);
        } else {
            $PreguntaSeguridad = PreguntaSeguridad::find($id);
        }

        if ($PreguntaSeguridad) {
            return array_merge($PreguntaSeguridad->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(PreguntaSeguridadRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
          
            $data = $request->all();
            $data['slug'] = str_slug($data['nombre'], '-');
           
            $PreguntaSeguridad = $id == 0 ? new PreguntaSeguridad() : PreguntaSeguridad::find($id);

            $PreguntaSeguridad->fill($data);
            $PreguntaSeguridad->save();


        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $PreguntaSeguridad->id,
            'texto' => $PreguntaSeguridad->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            PreguntaSeguridad::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            PreguntaSeguridad::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            PreguntaSeguridad::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = PreguntaSeguridad::select([
            'id', 'nombre', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}