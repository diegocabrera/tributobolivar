<?php

namespace App\Modules\Base\Http\Requests;

use App\Http\Requests\Request;

class RespuestaPreguntaRequest extends Request {
    protected $reglasArr = [
		'usuario_id' => ['required', 'integer'], 
		'pregunta_seguridad_id' => ['required', 'integer'], 
		'respuesta' => ['required']
	];
}