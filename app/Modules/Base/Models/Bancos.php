<?php
namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;

class Bancos extends modelo
{
    protected $table = 'bancos';
    protected $fillable = [
        'nombre',
        'codigo', 
        'correlativo'
    ];

    protected $campos = [
        'nombre' => [
            'type'        => 'text',
            'label'       => 'Nombre',
            'placeholder' => 'Nombre del Bancos'
        ], 
        'codigo' => [
            'type'        => 'text',
            'label'       => 'Codigo',
            'placeholder' => 'Bancos codigo'
        ],
        'correlativo' => [
            'type'        => 'text',
            'label'       => 'Correlativo',
            'placeholder' => 'Correlativo'
        ]
    ];
}