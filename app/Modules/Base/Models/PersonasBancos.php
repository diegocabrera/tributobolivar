<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;



class PersonasBancos extends modelo
{
    protected $table = 'personas_bancos';
    protected $fillable = ["personas_id","bancos_id","tipo_cuenta_id","cuenta",'digitos'];
    protected $campos = [
        'personas_id' => [
            'type'        => 'number',
            'label'       => 'Personas',
            'placeholder' => 'Personas del Personas Bancos'
        ],
        'bancos_id' => [
            'type'        => 'number',
            'label'       => 'Bancos',
            'placeholder' => 'Bancos del Personas Bancos'
        ],
        'tipo_cuenta_id' => [
            'type'        => 'number',
            'label'       => 'Tipo Cuenta',
            'placeholder' => 'Tipo Cuenta del Personas Bancos'
        ],
        'cuenta' => [
            'type'        => 'text',
            'label'       => 'Correo',
            'placeholder' => 'Correo del Personas Bancos'
        ],
        'digitos' => [
            'type'        => 'text',
            'label'       => 'digitos',
            'placeholder' => 'digitos del Personas Bancos'
        ]
    ];

    public function personas()
    {
        return $this->belongsTo('App\Modules\Base\Models\Personas', 'personas_id');
    }

    public function bancos()
    {
        return $this->belongsTo('App\Modules\Base\Models\Bancos', 'bancos_id');
    } 

    public function tipocuenta()
    {
        return $this->belongsTo('App\Modules\Base\Models\BancoTipoCuenta', 'tipo_cuenta_id');
    }
}