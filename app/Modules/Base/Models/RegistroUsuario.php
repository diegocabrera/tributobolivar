<?php

namespace App\Modules\Base\Models;

use App\Modules\Base\Models\Modelo;



class RegistroUsuario extends Modelo
{
    protected $table = 'registro-usuario';
    protected $fillable = ["tipo_persona","dni","nombres","estados_id","ciudades_id","municipios_id","parroquias_id","direccion","correo_pricipal","correo_secundario","telefono_casa","telefono_movil","telefono_oficina","password","rpassword","Pregunta_1","Respuesta_1","Pregunta_2","Respuesta_2","Pregunta_3","Respuesta_3"];
   // protected $campos = [
   
   /*     'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Pregunta Seguridad'
    ],
     'slug' => [
        'type' => 'text',
        'label' => 'Slug',
        'placeholder' => 'Slug del Pregunta Seguridad'
    ] */
//];


/*

           'dni'               => [' '], //['required', 'integer', 'unique:personas,dni'],
           'nombres'           => [' '], //['required', 'nombre', 'min:3', 'max:50'],
           'correo_pricipal'   => [' '], //['required','max:50', 'unique:personas_correo,correo'],
           'correo_secundario' => [' '], //['required','max:50', 'unique:personas_correo,correo'],
           'telefono_casa'     => [' '], //['required'],
           'telefono_movil'    => [' '], //['required'],
           'telefono_oficina'  => [' '], //['required'],
           'password'          => [' '], //['required', 'password', 'min:8', 'max:50'],
           'rpassword'         => [' '], //['required', 'password', 'min:8', 'max:50'],
           'Pregunta_1'        => [' '], //['required'],
           'Respuesta_1'       => [' '], //['required'],
           'Pregunta_2'        => [' '], //['required'],
           'Respuesta_2'       => [' '], //['required'],
           'Pregunta_3'        => [' '], //['required'],
           'Respuesta_3'       => [' '], //['required'],
*/
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}