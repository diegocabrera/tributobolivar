@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Pregunta Seguridad']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar PreguntaSeguridad.',
        'columnas' => [
            'Nombre' => '50',
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $PreguntaSeguridad->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection