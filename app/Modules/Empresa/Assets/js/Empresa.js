var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"rif","name":"rif"},
		{"data":"nombre","name":"nombre"},
		{"data":"abreviatura","name":"abreviatura"},
		{"data":"tlf","name":"tlf"},
		
		{"data":"direccion","name":"direccion"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});