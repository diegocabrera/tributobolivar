<?php

namespace App\Modules\Empresa\Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

use App\Modules\Empresa\Models\Empresa;
use App\Modules\Base\Models\Municipio;
class empresaseed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		DB::beginTransaction();
		try{
			$municipios = Municipio::where('estados_id', 6)->get();

			$num = 1;
			foreach ($municipios as $key => $value) {
				$empresa = Empresa::create([
					"rif" 			=> $num++ ,
					"nombre" 		=> $value->nombre,
					"abreviatura" 	=> $value->nombre,
					"tlf" 			=> '0',
					"direccion" 	=> $value->nombre
				]);
			}
			
		}catch(Exception $e){
			DB::rollback();
			echo "Error ";
		}
		DB::commit();
	
    }
}
