<?php

namespace App\Modules\Empresa\Http\Requests;

use App\Http\Requests\Request;

class EmpresaRequest extends Request {
    protected $reglasArr = [
		'rif' => ['required', 'min:3', 'max:80', 'unique:empresa,rif'], 
		'nombre' => ['required', 'min:3', 'max:80', 'unique:empresa,nombre'], 
		'abreviatura' => ['required', 'min:3', 'max:80', 'unique:empresa,abreviatura'], 
		'tlf' => ['required', 'min:3', 'max:200'], 
		'personas_id' => ['integer', 'min:3', 'max:200'], 
		'direccion' => ['min:3', 'max:200']
	];
}