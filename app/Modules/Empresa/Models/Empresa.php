<?php

namespace App\Modules\Empresa\Models;

use App\Modules\Base\Models\Modelo;

class Empresa extends Modelo
{
    protected $table = 'empresa';
    protected $fillable = ["rif","nombre","abreviatura","tlf","direccion"];
    protected $campos = [
        'rif' => [
            'type' => 'text',
            'label' => 'Rif',
            'placeholder' => 'Rif del Empresa'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre',
            'placeholder' => 'Nombre del Empresa'
        ],
        'abreviatura' => [
            'type' => 'text',
            'label' => 'Abreviatura',
            'placeholder' => 'Abreviatura del Empresa'
        ],
        'tlf' => [
            'type' => 'text',
            'label' => 'Tlf',
            'placeholder' => 'Tlf del Empresa'
        ],
    
        'direccion' => [
            'type' => 'textarea',
            'label' => 'Direccion',
            'placeholder' => 'Direccion del Empresa',
            'cont_class' => 'col-sm-12'
        ]
    ];
}
