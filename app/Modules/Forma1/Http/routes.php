<?php

Route::group(['middleware' => 'web', 'prefix' =>Config::get('admin.prefix') , 'namespace' => 'App\\Modules\Forma1\Http\Controllers'], function()
{
    //Route::get('/', 'UserPerfilController@index');

    	/**
         * pago de forma
         */
	
	Route::group(['prefix' => 'forma1'], function() {
		Route::get('/', 		    'Forma1Controller@index');	
		
	});

	Route::group(['prefix' => 'usuario'], function() {
		Route::get('/', 		    'UserPerfilController@index');
		
	});

});
