@extends(isset($layouts) ? $layouts : 'base::layouts.default') 

@section('content-top') 



@include('base::partials.ubicacion', ['ubicacion' => ['Estados']]) 

@endsection 

@section('content')


<div class="row">

    <div class="container">
        <!-- blue -->
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-building"></i>
                </div>
            </div>
            <div class="portlet-body">

                <div class="table-responsive">

                        <div class="container-tablet">
                            <!-- container -->
                            <span style="color:red;position: absolute;padding-left: 12%;">forma 001 </span>
                            <div class="col-lg-3">
                                <!-- col-lg-3 -->
                                <div class="main">
                                    <!-- main -->
                        
                                    <img src="{{ url('public/img/logo.png') }}">
                                    <!-- <p class="lead"> la url. del documento</p> -->
                                    
                        
                                </div>
                                <!-- fin main -->
                            </div>
                            <!-- col 6 -->
                            <div class="col-lg-6"  style="position:  absolute;padding-left:  18%;"> <!-- col 6 -->
                                    <div class="A" style="text-align:center">
                                        PLANILLA PARA EL PAGO DE LAS TASAS ESTABLECIDAS EN LA<br> LEY DE TIMBRE FISCAL DEL ESTAO BOLIVAR  
                                    </div>

                                    <div class="B" style="text-align:center;width: 106%;">
                                       <span style="color:red;text-align:center;margin-top:  10%;">
                                             Servicio Autonomo de Administracion Tributaria del Estado Bolivar<br>RIF:G20003739-3
                                       </span>
                                    </div>
                                    
                            </div> <!-- fin col 6 -->
                            <div>
                                    <p style="margin-left: 60%;margin-top: 2%;">SAAT-01</p>
                            </div>
                            
                                  
                            
                            
                            <div class="col-lg-1">
                                <!-- col 3 -->
                                <div class="main">
                        
                                </div>
                                <!-- fin main -->
                            </div>
                            <!-- col 3 -->
                        </div>
                        <!-- fin del contaimer -->

                        <p style="margin-left: 60%;margin-top: -5%;float: right;margin-right: 28%;">N°</p>

                        <div class="br" style="margin-top: 6%;"></div>

                    <table width="100%" height="318" border="1" summary="eee">

                        <tr>
                            <th width="78" scope="col">
                                <span>COD. CONT.</span>
                            </th>
                            <th width="58" scope="col">
                                <span>ART.</span>
                            </th>
                            <th width="632" scope="col">
                                <span>DESCRIPCION</span>
                            </th>
                            <th width="39" scope="col">
                                <span>COD.</span>
                            </th>
                            <th width="66" scope="col">
                                <span>MONTO A PAGAR</span>
                            </th>
                        </tr>
                        <tr>
                            <td scope="row">
                                <span>301100132
                                    <p></p>
                                </span>
                            </td>
                            <td scope="row" style="color:red; text-align:center">14</td>
                            <td>
                                
                                    <p>INSTALACION TRAMITES Y RENOVACION DE REGISTRO Y AUTORIZACION DE LICORES </p>
                                
                            <p>
                                </p><p></p>
                            <p></p>
                            <div class="container3">
                                <div class="super-container1" style="position:static;left:79.1%;float:left;width: 100%;padding-left: 12%;">
                                    <div class="container" style="position:static;margin-left: 0%;float:left;width: 5%;">
                                        <!-- primera tabla -->
                                        <table border="1" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                                <tr>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            C
                                                        </span>
                                                    </td>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> <!-- container --> 

                                    <div class="container" style="position:static;float:left;width: 5%;">
                                        <table border="1" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            Cc
                                                        </span>
                                                    </td>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                    <div class="container" style="position:static;float:left;width: 1%;padding-left: 3%;">
                                        <table border="1" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            Cv
                                                        </span>
                                                    </td>
                                                    <td width="6" valign="top">
                                                        <span>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    
                                </div>
                                <!-- fin primera tabla -->
                                <div class="super-container" style="position:static;left:79.1%;float:left;width:150px;">
                                    <!-- segunda tabla -->
                                    <div class="container" style="position:  static;float:left;padding-left: 20%;width: 29%;">
                                        <table border="1" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="6" valign="top">
                                                        <span>
                                                            MN
                                                        </span>
                                                    </td>
                                                    <td width="6" valign="top">

                                                        <span>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--fin container -->

                                    <!-- la otra tabla  -->
                                    <div class="container" style="float:left;padding-left: 26%;width: 40px;">

                                        <table border="1" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="6" valign="top">
                                                        <span>
                                                           MY
                                                        </span>
                                                    </td>
                                                    <td width="6" valign="top">
                                                        <span>
                                                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div> <!-- fin container -->

                                </div> <!-- fin del super container -->

                                <div class="super-container" style="position:static;left:79.1%;float:left;width: 18%;">
                                    <div class="n" style="float:left;">
                                        <span>
                                            <p> N° </p>
                                        </span>
                                    </div>
                                    <table border="1" cellspacing="0" cellpadding="0">
                                        <tbody>
                                            <tr>
                                                <td width="33" valign="top">
                                                   <span>
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </span>
                                                </td>
                                                <td width="41" valign="top">
                                                   <span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </span>
                                                </td>
                                                <td width="32" valign="top">
                                                   <span>
                                                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </span>
                                                </td>
                                                <td width="32" valign="top">
                                                   <span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </span>
                                                </td>
                                                <td width="32" valign="top">
                                                   <span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!-- fin del super-container -->
                            
                            </div> <!-- fin del super container1 -->
                            
                            </div><!-- fin container3 -->

                         
                            <br>
                            <br>
                            </td>
                                <td style="color:red;text-align:right;"> 101 </td>
                                <td> &nbsp; </td>

                                <!-- otra tabla -->
                                <tbody>

                                    <tr>
                                        <td width="33">
                                            <p>
                                                301100132
                                            </p>


                                        </td>
                                        <td width="41" style="color:red;text-align:center;">
                                            <p>
                                                24
                                            </p>
                                        </td>
                                        <td width="32" valign="top">
                                            <p>
                                                PAGARES BANCARIOS O LETRAS DE CAMBIO POR EL TOTAL EMITIDO (1/1000)
                                            </p>
                                        </td>
                                       
                                        <td width="32" valign="top" style="color:red;text-align:right;">
                                            <p>
                                                102
                                            </p>
                                        </td>
                                        <td width="32" valign="top">
                                            <p>
                                                <span>
                                                    <p>&nbsp;</p>
                                                </span>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>

                                <!-- fin otra tabla -->


                                <tr>
                                    <td width="33">

                                        <p>
                                            301100132
                                        </p>
                                    </td>

                                    <td style="color:red;text-align:center">
                                        <p>
                                            24
                                        </p>
                                    </td>

                                    <td>
                                        <span>
                                            <p>ORDENES DE PAGO POR SERVICIOS PRESTADOS AL SECTOR PUBLICO (1/1000)</p>
                                        </span>
                                    </td>
                                    <td width="32" valign="top" style="color:red;text-align:right;">
                                        <p>
                                            103
                                        </p>
                                        <td>&nbsp;</td>
                                </tr>

                                <!-- AS-->

                                <tr>

                                    <td width="33">

                                        <p>
                                            301100132
                                        </p>
                                    </td>

                                    <td style="color:red;text-align:center">
                                        <p>
                                            22
                                        </p>
                                    </td>

                                    <td>
                                        <span>
                                            <p>POR LOS SERVICION TECNICOS FORESTALES</p>
                                        </span>
                                    </td>

                                    <td style="color:red;text-align:right;">
                                        <P>
                                            104
                                        </P>
                                    </td>
                                    <td> </td>
                                </tr>

                                <!-- BB -->

                                <tr>

                                    <td width="33">

                                        <p>
                                            301100132
                                        </p>
                                    </td>

                                    <td style="color:red;text-align:center">
                                        <p>
                                            22
                                        </p>
                                    </td>

                                    <td>
                                        <span>
                                            <p>POR ACTIVIDADES DE EXPLORACION Y EXTRACCION DE MINERALES METALICOS Y NO METALICOS</p>
                                        </span>
                                    </td>

                                    <td style="color:red;text-align:right;">
                                        <p>
                                            105
                                        </p>

                                    </td>
                                    <td> </td>
                                </tr>

                                <!-- CC -->

                                <tr>

                                    <td width="33">

                                        <p>
                                            301100132
                                        </p>
                                    </td>

                                    <td style="color:red;text-align:center">
                                        <p>
                                            20
                                        </p>
                                    </td>

                                    <td>
                                        <span>
                                            <p>POR SERVICIOS Y DOCUMENTOS SANITARIOS</p>
                                        </span>
                                    </td>

                                    <td style="color:red;text-align:right;">
                                        <p>
                                            121
                                        </p>
                                    </td>
                                    <td> </td>
                                </tr>

                                <!-- DD -->

                                <tr>

                                    <td width="33">

                                        <p>
                                            301100132
                                        </p>
                                    </td>

                                    <td style="color:red;text-align:center">
                                        <p>

                                        </p>
                                    </td>

                                    <td>
                                        <span>
                                            <p>OTROS ESPECIFIQUE</p>
                                        </span>
                                    </td>

                                    <td style="color:red;text-align:right;">
                                        <p>
                                            115
                                        </p>
                                    </td>
                                    <td> </td>
                                </tr>

                                <!-- EE -->

                                <tr>

                                    <td colspan="4" style="text-align: right;">

                                        <p>
                                            sub total a pagar ----------------------------------------------------------------------------------------------------------------&gt;
                                        </p>
                                    </td>

                                    <td>*</td>
                                </tr>

                    </table>  <!-- fin de table -->

                </div> <!-- table-responsive   -->

            </div> <!-- portlet-body  -->
        </div> <!-- portlet box blue  -->


        <div class="container">
            <div class="efectivo jumbotron" style="position:static;float:right;margin-right:4%;background: #fff;">
                    
                <div class="n" style="float:left;padding-top:5px;">
                    <span>
                        efectivo:
                    </span>
                </div>

                <div class="pago" style="position:static; float:right;">

                    <table border="1" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td width="100px">

                                    <input type="text">

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- fin del efectivo -->
        </div>
        <!-- fin container -->

        <div class="container jumbotron" style="padding-top:12px;">

            <table border="1" cellspacing="0" cellpadding="0" style="width: 80%;">
                <tbody>

                    <tr>
                        <td width="6" valign="top" style="width: 25%;">
                            <p> BANCO </p>
                        </td>
                        <td width="6" valign="top" style="width: 50%;">

                            <p>SERIAL DEL CHEQUE DE GERENCIA</p>
                        </td>
                        <td width="6" valign="top" style="width: 25%;">

                            <p>MONTO</p>
                        </td>

                    </tr>
                    <tr>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>

                    </tr>
                    <tr>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>
                        <td valign="top">&nbsp;</td>

                    </tr>
                </tbody>

            </table>

            

            <div class="montojumbotron" style="position:static;float:right;margin-right:4%;background: #fff;"><!-- monto -->

                <div class="e" style="float:left;">
                    <span>
                        <p style="padding-top: 9px;">
                            efectivo:
                            <br> efectivo:
                        </p>
                    </span>
                </div>

                <table border="1" style="margin-top: 20px;position: static;float: right;">


                    <tbody>
                        <tr>
                            <th style="width: 100px;">
                                <input type="text">
                            </th>
                        </tr>

                        <tr>
                            <th style="width: 100px;">
                                <input type="text">
                            </th>
                        </tr>

                    </tbody>
                </table>

            </div>
            <!-- fin del monto -->

        </div>
        <!-- fin del container -->

        <div class="col-md-12"></div>
        <!-- col 6 -->

        <div class="container">
            <!-- container -->
            <div class="col-lg-3">
                <!-- col 6 -->
                <div class="main">
                    <!-- main -->
                    <div class="jumbotron" style="background: #fff;">
                        <!-- <img src="{{ url('public/img/qr.png') }}" style="width: 75%;"> -->
                        <p class="lead"> la url. del documento</p>

                    </div>
                </div>
                <!-- fin main -->
            </div>
            <!-- col 6 -->
            <div class="col-lg-6">

            </div>

            <div class="col-lg-3">
                <!-- col 6 -->
                <div class="main">
                    <!-- main -->
                    <div class="jumbotron" style="background: #fff;">
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            <br> 
                            
                        </div>
                </div>
                <!-- fin main -->
            </div>
            <!-- col 6 -->

        </div>
        <!-- fin del contaimer -->

        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-building"></i>Bordered Bootstrap 3.0 Responsive Table </div>

            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Table heading </th>
                                <th> Table heading </th>
                                <th> Table heading </th>
                                <th> Table heading </th>
                                <th> Table heading </th>
                                <th> Table heading </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> 1 </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                            </tr>
                            <tr>
                                <td> 2 </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                            </tr>
                            <tr>
                                <td> 3 </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                                <td> Table cell </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div> <!-- fin del container -->

</div> <!-- fin del row -->
@endsection