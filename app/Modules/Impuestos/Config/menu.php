<?php
$menu['impuestos'] = [
	[
		'nombre' 	=> 'Impuestos',
		'direccion' => '#Pago',
		'icono' 	=> 'fa fa-building',
		'menu' 		=> [
				[
					'nombre' 	=> 'Articulos',
					'direccion' => 'impuestos/articulos',
					'icono' 	=> 'fa fa-building'
				],
				[
					'nombre' 	=> 'UT',
					'direccion' => 'impuestos/ut',
					'icono' 	=> 'fa fa-building'
				],
				[
					'nombre' 	=> 'Impuestos',
					'direccion' => 'impuestos',
					'icono' 	=> 'fa fa-building'
				],
		]
	]
];
