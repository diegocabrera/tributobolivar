<?php

namespace App\Modules\Impuestos\Http\Controllers;

//Controlador Padre
use App\Modules\Impuestos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Impuestos\Http\Requests\ArticulosRequest;

//Modelos
use App\Modules\Impuestos\Models\Articulos;

class ArticulosController extends Controller
{
    protected $titulo = 'Articulos';

    public $js = [
        'Articulos'
    ];
    
    public $css = [
        'Articulos'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('impuestos::Articulos', [
            'Articulos' => new Articulos()
        ]);
    }

    public function nuevo()
    {
        $Articulos = new Articulos();
        return $this->view('impuestos::Articulos', [
            'layouts' => 'base::layouts.popup',
            'Articulos' => $Articulos
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Articulos = Articulos::find($id);
        return $this->view('impuestos::Articulos', [
            'layouts' => 'base::layouts.popup',
            'Articulos' => $Articulos
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Articulos = Articulos::withTrashed()->find($id);
        } else {
            $Articulos = Articulos::find($id);
        }

        if ($Articulos) {
            return array_merge($Articulos->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ArticulosRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Articulos = $id == 0 ? new Articulos() : Articulos::find($id);

            $Articulos->fill($request->all());
            $Articulos->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Articulos->id,
            'texto' => $Articulos->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Articulos::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Articulos::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Articulos::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Articulos::select([
            'id', 'codigo', 'descripcion', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}