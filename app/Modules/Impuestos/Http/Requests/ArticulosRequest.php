<?php

namespace App\Modules\Impuestos\Http\Requests;

use App\Http\Requests\Request;

class ArticulosRequest extends Request {
    protected $reglasArr = [
		'codigo' => ['required']
	];
}
