<?php

namespace App\Modules\Impuestos\Http\Requests;

use App\Http\Requests\Request;

class ImpuestosRequest extends Request {
    protected $reglasArr = [
		'impuesto' => ['required',],
		'articulo_id' => ['required', 'integer'],
		'seccion_articulo_id' => ['integer'],
		'codigo' => ['required',]
	];
}
