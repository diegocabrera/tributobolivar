<?php

namespace App\Modules\Impuestos\Http\Requests;

use App\Http\Requests\Request;

class UnidadTributariaRequest extends Request {
    protected $reglasArr = [
		'periodo' => ['required','unique:unidad_tributaria,periodo'], 
		'costo' => ['required']
	];
}
