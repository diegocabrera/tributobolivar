<?php

Route::group(['middleware' => 'web', 'prefix' => 'impuestos', 'namespace' => 'App\\Modules\Impuestos\Http\Controllers'], function()
{
    Route::get('/',                 'ImpuestosController@index');
    Route::get('nuevo',             'ImpuestosController@nuevo');
    Route::get('cambiar/{id}',      'ImpuestosController@cambiar');

    Route::get('buscar/{id}',       'ImpuestosController@buscar');

    Route::post('guardar',          'ImpuestosController@guardar');
    Route::put('guardar/{id}',      'ImpuestosController@guardar');

    Route::delete('eliminar/{id}',  'ImpuestosController@eliminar');
    Route::post('restaurar/{id}',   'ImpuestosController@restaurar');
    Route::delete('destruir/{id}',  'ImpuestosController@destruir');

    Route::get('datatable',         'ImpuestosController@datatable');




        Route::group(['prefix' => 'ut'], function() {
            Route::get('/',                 'UnidadTributariaController@index');
            Route::get('nuevo',             'UnidadTributariaController@nuevo');
            Route::get('cambiar/{id}',      'UnidadTributariaController@cambiar');

            Route::get('buscar/{id}',       'UnidadTributariaController@buscar');

            Route::post('guardar',          'UnidadTributariaController@guardar');
            Route::put('guardar/{id}',      'UnidadTributariaController@guardar');

            Route::delete('eliminar/{id}',  'UnidadTributariaController@eliminar');
            Route::post('restaurar/{id}',   'UnidadTributariaController@restaurar');
            Route::delete('destruir/{id}',  'UnidadTributariaController@destruir');

            Route::get('datatable',         'UnidadTributariaController@datatable');
        });


        Route::group(['prefix' => 'articulos'], function() {
            Route::get('/',                 'ArticulosController@index');
            Route::get('nuevo',             'ArticulosController@nuevo');
            Route::get('cambiar/{id}',      'ArticulosController@cambiar');

            Route::get('buscar/{id}',       'ArticulosController@buscar');

            Route::post('guardar',          'ArticulosController@guardar');
            Route::put('guardar/{id}',      'ArticulosController@guardar');

            Route::delete('eliminar/{id}',  'ArticulosController@eliminar');
            Route::post('restaurar/{id}',   'ArticulosController@restaurar');
            Route::delete('destruir/{id}',  'ArticulosController@destruir');

            Route::get('datatable',         'ArticulosController@datatable');
        });


        Route::group(['prefix' => 'seccion_articulos'], function() {
            Route::get('/',                 'SeccionArticulosController@index');
            Route::get('nuevo',             'SeccionArticulosController@nuevo');
            Route::get('cambiar/{id}',      'SeccionArticulosController@cambiar');

            Route::get('buscar/{id}',       'SeccionArticulosController@buscar');

            Route::post('guardar',          'SeccionArticulosController@guardar');
            Route::put('guardar/{id}',      'SeccionArticulosController@guardar');

            Route::delete('eliminar/{id}',  'SeccionArticulosController@eliminar');
            Route::post('restaurar/{id}',   'SeccionArticulosController@restaurar');
            Route::delete('destruir/{id}',  'SeccionArticulosController@destruir');

            Route::get('datatable',         'SeccionArticulosController@datatable');
        });

    //{{route}}
});
