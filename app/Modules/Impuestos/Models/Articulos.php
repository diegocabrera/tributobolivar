<?php

namespace App\Modules\Impuestos\Models;

use App\Modules\Base\Models\Modelo;



class Articulos extends Modelo
{
    protected $table = 'articulos';
    protected $fillable = ["codigo","descripcion"];
    protected $campos = [
    'codigo' => [
        'type' => 'text',
        'label' => 'Codigo',
        'placeholder' => 'Codigo del Articulos'
    ],
    'descripcion' => [
        'type' => 'textarea',
        'label' => 'Descripcion',
        'placeholder' => 'Descripcion del Articulos',
        'cont_class' => 'col-lg-12 col-md-12 col-sm-12 col-xs-12'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}