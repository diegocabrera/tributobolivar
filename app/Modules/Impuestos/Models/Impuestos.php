<?php

namespace App\Modules\Impuestos\Models;

use App\Modules\Base\Models\Modelo;
use App\Modules\Impuestos\Models\Articulos;
use App\Modules\Impuestos\Models\SeccionArticulos;



class Impuestos extends Modelo
{
    protected $table = 'impuestos';
    protected $fillable = ["impuesto","articulo_id","seccion_articulo_id","codigo"];
    protected $campos = [
    'impuesto' => [
        'type' => 'text',
        'label' => 'Impuesto',
        'placeholder' => 'Impuesto del Impuestos'
    ],
    'articulo_id' => [
        'type' => 'select',
        'label' => 'Articulo',
        'placeholder' => '- Seleccione un Articulo',
        'url' => 'Agrega una URL Aqui!'
    ],
    'codigo' => [
        'type' => 'text',
        'label' => 'Codigo',
        'placeholder' => 'Codigo del Impuestos'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['articulo_id']['options'] = Articulos::pluck('codigo', 'id');
        

    }

    public function articulos()
        {
            return $this->hasOne('App\Modules\Impuestos\Models\Articulos');
        }

}
