<?php

namespace App\Modules\Impuestos\Models;

use App\Modules\Base\Models\Modelo;



class UnidadTributaria extends Modelo
{
    protected $table = 'unidad_tributaria';
    protected $fillable = ["periodo","costo"];
    protected $campos = [
    'periodo' => [
        'type' => 'text',
        'label' => 'Periodo',
        'placeholder' => 'Periodo del Unidad Tributaria',
        'cont_class' => 'col-lg-3 col-md-3 col-sm-6 col-xs-12'
    ],
    'costo' => [
        'type' => 'number',
        'label' => 'Costo',
        'placeholder' => 'Costo del Unidad Tributaria'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

    }


}
