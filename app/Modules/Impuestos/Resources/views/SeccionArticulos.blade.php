@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Seccion Articulos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar SeccionArticulos.',
        'columnas' => [
            'Codigo' => '25',
		'Descripcion' => '25',
		'Costo' => '25',
		'Articulo' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $SeccionArticulos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection