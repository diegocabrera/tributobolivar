@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')

    @include('base::partials.ubicacion', ['ubicacion' => ['Unidad Tributaria']])

    @include('base::partials.modal-busqueda', [
            'titulo' => 'Buscar UnidadTributaria.',
            'columnas' => [
                'Periodo' => '50',
        		'Costo' => '50'
            ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $UnidadTributaria->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection
