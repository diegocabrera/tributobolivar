<?php
$menu['pagos'] = [
	[
		'nombre' 	=> 'Pago',
		'direccion' => '#Pago',
		'icono' 	=> 'fa fa-building',
		'menu' 		=> [
				[
					'nombre' 	=> 'Pagos',
					'direccion' => 'pagos',
					'icono' 	=> 'fa fa-building'
				],
				[
					'nombre' 	=> 'Referencias',
					'direccion' => 'pagos/referencias',
					'icono' 	=> 'fa fa-building'
				]
		]
	]
];
