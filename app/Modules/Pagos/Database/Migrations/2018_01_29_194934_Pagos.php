<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Hay que crear una base de datos para la informaciòn otorgada por el/los Bancos
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('impuesto_id')->unsigned()->nulleable();
            $table->integer('app_usuario_id')->unsigned()->nulleable();
            $table->string('codigo');
            $table->string('referencia')->nullable();
            $table->integer('monto');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('impuesto_id')
                ->references('id')->on('impuestos')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('app_usuario_id')
                ->references('id')->on('app_usuario')
                ->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('referencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('monto');
            $table->string('numero');
            $table->string('banco');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referencias');
        Schema::dropIfExists('pagos');
    }
}
