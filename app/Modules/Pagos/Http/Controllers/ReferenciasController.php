<?php

namespace App\Modules\Pagos\Http\Controllers;

//Controlador Padre
use App\Modules\Pagos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Pagos\Http\Requests\ReferenciasRequest;

//Modelos
use App\Modules\Pagos\Models\Referencias;

class ReferenciasController extends Controller
{
    protected $titulo = 'Referencias';

    public $js = [
        'Referencias'
    ];
    
    public $css = [
        'Referencias'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('pagos::Referencias', [
            'Referencias' => new Referencias()
        ]);
    }

    public function nuevo()
    {
        $Referencias = new Referencias();
        return $this->view('pagos::Referencias', [
            'layouts' => 'base::layouts.popup',
            'Referencias' => $Referencias
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Referencias = Referencias::find($id);
        return $this->view('pagos::Referencias', [
            'layouts' => 'base::layouts.popup',
            'Referencias' => $Referencias
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Referencias = Referencias::withTrashed()->find($id);
        } else {
            $Referencias = Referencias::find($id);
        }

        if ($Referencias) {
            return array_merge($Referencias->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ReferenciasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Referencias = $id == 0 ? new Referencias() : Referencias::find($id);

            $Referencias->fill($request->all());
            $Referencias->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Referencias->id,
            'texto' => $Referencias->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Referencias::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Referencias::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Referencias::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Referencias::select([
            'id', 'monto', 'numero', 'banco', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}