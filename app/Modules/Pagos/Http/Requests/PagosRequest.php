<?php

namespace App\Modules\Pagos\Http\Requests;

use App\Http\Requests\Request;

class PagosRequest extends Request {
    protected $reglasArr = [
		'impuesto_id' => ['required', 'integer'],
		'app_usuario_id' => ['required', 'integer'], 
		'codigo' => ['required', 'min:3', 'max:191'],
		'referencia' => ['required', 'min:3', 'max:191']
	];
}
