<?php

namespace App\Modules\Pagos\Http\Requests;

use App\Http\Requests\Request;

class ReferenciasRequest extends Request {
    protected $reglasArr = [
		'monto' => ['required', 'min:3', 'max:191'], 
		'numero' => ['required', 'min:3', 'max:191'], 
		'banco' => ['required', 'min:3', 'max:191']
	];
}