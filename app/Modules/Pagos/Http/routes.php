<?php

Route::group(['middleware' => 'web', 'prefix' => 'pagos', 'namespace' => 'App\\Modules\Pagos\Http\Controllers'], function()
{
    Route::get('/',                 'PagosController@index');
    Route::get('nuevo',             'PagosController@nuevo');
    Route::get('cambiar/{id}',      'PagosController@cambiar');

    Route::get('buscar/{id}',       'PagosController@buscar');

    Route::post('guardar',          'PagosController@guardar');
    Route::put('guardar/{id}',      'PagosController@guardar');

    Route::delete('eliminar/{id}',  'PagosController@eliminar');
    Route::post('restaurar/{id}',   'PagosController@restaurar');
    Route::delete('destruir/{id}',  'PagosController@destruir');

    Route::get('datatable',         'PagosController@datatable');


        Route::group(['prefix' => 'referencias'], function() {
            Route::get('/',                 'ReferenciasController@index');
            Route::get('nuevo',             'ReferenciasController@nuevo');
            Route::get('cambiar/{id}',      'ReferenciasController@cambiar');

            Route::get('buscar/{id}',       'ReferenciasController@buscar');

            Route::post('guardar',          'ReferenciasController@guardar');
            Route::put('guardar/{id}',      'ReferenciasController@guardar');

            Route::delete('eliminar/{id}',  'ReferenciasController@eliminar');
            Route::post('restaurar/{id}',   'ReferenciasController@restaurar');
            Route::delete('destruir/{id}',  'ReferenciasController@destruir');

            Route::get('datatable',         'ReferenciasController@datatable');
        });

    //{{route}}
});
