@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Pagos']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Pagos.',
        'columnas' => [
            'Impuesto' => '25',
		'Usuario' => '25',
		'Codigo' => '25',
		'Referencia' => '25'
        ]
    ])
@endsection

@section('content')
    <div class="row">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Pagos->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection