  <?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-4">


        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">0</span>
                        <small class="font-green-sharp"></small>
                    </h3>
                    <small>TOTAL DE FORMAS 001 </small>
                </div>
                <div class="icon">
                    <i class="fa fa-file-pdf-o"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 76%;" class="progress-bar progress-bar-success green-sharp">
                                                    <span class="sr-only">76% progreso</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> progreso </div>
                    <div class="status-number"> 76% </div>
                </div>
            </div>
        </div>




    </div>
    <div class="col-md-4">


        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="7800">0</span>
                        <small class="font-purple-soft"></small>
                    </h3>
                    <small>TOTAL DE FORMAS 005</small>
                </div>
                <div class="icon">
                    <i class="fa fa-file-pdf-o"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 76%;" class="progress-bar progress-bar-success purple-soft">
                                                    <span class="sr-only">76% progreso</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> progreso </div>
                    <div class="status-number"> 76% </div>
                </div>
            </div>
        </div>




    </div>
    <div class="col-md-4">


        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="7800">0</span>
                        <small class="font-red-haze"></small>
                    </h3>
                    <small>TOTAL DE FORMAS 003</small>
                </div>
                <div class="icon">
                    <i class="fa fa-file-pdf-o"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 76%;" class="progress-bar progress-bar-success red-haze">
                                                    <span class="sr-only">76% progreso</span>
                    </span>
                </div>
                <div class="status">
                    <div class="status-title"> progreso </div>
                    <div class="status-number"> 76% </div>
                </div>
            </div>
        </div>




    </div>
</div>


<div>
    <div class="col-md-3">
        <div class="list-group">
            <a href="#" class="list-group-item active" style="background:#2f353b; border:#2f353b">
                <center>Opciones</center>
            </a>
            <a href="" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i>Opcion 1</a>
            <a href="" class="list-group-item"><i class="fa fa-plus" aria-hidden="true"></i>opcion 2</a>
            <a href="" class="list-group-item "><i class="fa fa-search" aria-hidden="true"></i> Opcion 3</a>



        </div>
    </div>

</div>


<div class="col-md-8">
    <div id="grafica"></div>

    <div class="jumbotron" style="background: #fff;" align="center">
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>

</div>



</div>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>