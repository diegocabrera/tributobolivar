 

<?php $__env->startSection('content-top'); ?> 



<?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Estados']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>


<div class="row">

    <div class="container">

        <!-- col 12 -->
        <div class="col-12">
            
            <div class="col-lg-3">
                <form role="search" method="get" action="">
                            
                    <div class="input-group">
                        <center>
                        <input type="text" class="form-control"
                          onkeyup="this.value = this.value.toUpperCase();"
                          name="search" placeholder="CI ">
                        </center>
                        <span class="input-group-btn">
                            <!-- <button type="submit" class="btn btn-md"> buscar
                                <span class="glyphicon glyphicon-search"> </span>
                            </button> -->
                        </span>
                    </div>           
                </form>
            </div>
            <div class="col-lg-8">

                <?php echo e(Form::bsText('102', '', [
                    'class'       => 'form-control',
                    'id' 		  => '102',
                    'label'       => 'PAGARES BANCARIOS O LETRAS DE CAMBIO POR EL TOTAL EMITIDO (1/1000):',
                    'class_cont'  => 'col-md-12',
                    'placeholder' => '',
                    'required' => 'required'                                              
                ])); ?>



                <?php echo e(Form::bsText('103', '', [
                    'class'       => 'form-control',
                    'id' 		  => '103',	
                    'label'       => 'ORDENES DE PAGO POR SERVICIOS PRESTADOS AL SECTOR PUBLICO (1/1000):',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required' 
                                                                    
                ])); ?>


                <?php echo e(Form::bsText('104', '', [
                    'class'       => 'form-control',
                    'id' 		  => '104',	
                    'label'       => 'POR LOS SERVICION TECNICOS FORESTALES:',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required' 
                                                                
                ])); ?>



                <?php echo e(Form::bsText('105', '', [
                    'class'       => 'form-control',
                    'id' 		  => '105',	
                    'label'       => 'POR ACTIVIDADES DE EXPLORACION Y EXTRACCION DE MINERALES METALICOS Y NO METALICOS:',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required'                                          
                ])); ?>


                <?php echo e(Form::bsText('121', '', [
                    'class'       => 'form-control',
                    'id' 		  => '121',	
                    'label'       => 'POR SERVICIOS Y DOCUMENTOS SANITARIOS:',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required'                                          
                ])); ?>


                <?php echo e(Form::bsText('115', '', [
                    'class'       => 'form-control',
                    'id' 		  => '115',	
                    'label'       => 'OTROS ESPECIFIQUE:',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required'                                          
                ])); ?>


                <button type="" class="btn green button-submit" id="guardar"> Genere pdf forma001  <i class="fa fa-check"></i></button>

            </div>

        </div>  <!-- col 12 -->

    </div> <!-- fin del container -->

</div> <!-- fin del row -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>