 

<?php $__env->startSection('content-top'); ?> 

<?php echo $__env->make('base::partials.ubicacion', ['ubicacion' => ['Estados']], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<?php $__env->stopSection(); ?> 

<?php $__env->startSection('content'); ?>
    
    <div class="row">

        <div class="container">

            <p>omar</p>

           <?php echo e(Form::bsText( '105', '', [
            'type'       => 'select',
            'label'      => 'Tipo Persona',
            'url'        => 'personas/tipopersona',
            'cont_class' => 'form-group col-md-3'
        ])); ?>



         <?php echo e(Form::bsText('105', '', [
                    'class'       => 'form-control',
                    'id' 		  => '105',	
                    'label'       => 'POR ACTIVIDADES DE EXPLORACION Y EXTRACCION DE MINERALES METALICOS Y NO METALICOS:',	
                    'placeholder' => '',
                    'class_cont'  => 'col-md-12',
                    'required' => 'required'                                          
                ])); ?> 



        <!-- 'dni' => [
            'type'        => 'text',
            'label'       => 'C.I',
            'placeholder' => 'C.I del Personas',
            'cont_class'  => 'form-group col-md-3'
        ],
        'nombres' => [
            'type'        => 'text',
            'label'       => 'Nombres y Apellidos',
            'placeholder' => 'Nombres y Apellidos del Personas',
            'cont_class'  => 'form-group col-md-6'
        ] -->

        <?php echo Form::open(['id' => 'submit_form', 'name' => 'formulario100', 'method' => 'POST' ]); ?>

			
            <div class="col-md-12">
                <div class="portlet light" id="form_wizard_1">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class=" icon-layers font-red"></i>
                            <span class="caption-subject font-red bold uppercase">Registro de Nuevo Usuario
                                <span class="step-title"> 1 de 4 </span>
                            </span>
                        </div>
                        <div class="close" >
                            <a href="javascript:;" class="" id="cerrar" style="font-size: 37px; color:#ff0a1a;">
                                <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                            </a>	
                        </div>
                    </div>
                    <div class="portlet-body form"> 
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step">
                                            <span class="number"> 1 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i>Datos Generales</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab" class="step">
                                            <span class="number"> 2 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i>Recidencia</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab" class="step active">
                                            <span class="number"> 3 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i>contacto</span>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step active">
                                            <span class="number"> 4 </span>
                                            <span class="desc">
                                                <i class="fa fa-check"></i> Seguridad </span>
                                        </a>
                                    </li>
                                    
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> Usted tiene algunos errores en el formulario. Por favor, compruebe.
                                    </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> ¡La validación de su formulario es exitosa!
                                    </div>
                                    
                                    <div class="tab-pane active" id="tab1">
                                        <div class="panel ">
                                            
                                            <div class="panel-body">
                                                
                                            <div class="form-group col-md-4 cont-persona">
                                            <label for="nombres">Rif / Numero Cedula:</label>
                                            <div class="form-group multiple-form-group input-group">
                                            
                                                < <div class="input-group-btn input-group-select">
                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="concept">-</span><span class="caret"></span></button>
                                                    <ul class="dropdown-menu" role="menu" required >
                                                       
                                                    </ul>
                                                    <!-- RegistroRequest  -->
                                                    <input id="tipo_persona" name="tipo_persona" class="input-group-select-val" type="hidden" />
                                                </div>  

                                                    <?php echo e(Form::text('dni', '', [
                                                        'id'          => 'dni',
                                                        'class'       => 'form-control',
                                                        'placeholder' => '',
                                                        'required' => 'required'
                                                    ])); ?>

                                                    
                                            </div>
                                        </div>
                                        <?php echo e(Form::bsText('nombres', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Nombres Completo / Razon Social:',
                                            'class_cont'  => 'col-md-8',
                                            'placeholder' => '',
                                            'required' => 'required'
                                        ])); ?>


                                        </div>
                                    </div>
                    

                                    </div>

                                    <div class="tab-pane" id="tab2">
                                          
                                    </div>
                                    
                                    <div class="tab-pane" id="tab3">
                                        
                                        <?php echo e(Form::bsEmail('correo_pricipal', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Correo Principal',
                                            'class_cont'  => 'col-md-6',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>


                                        <?php echo e(Form::bsEmail('correo_secundario', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Correo Secundario:',
                                            'class_cont'  => 'col-md-6',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>

                                        <div class="col-md-12"></div>

                                        <?php echo e(Form::bsText('telefono_casa', '', [
                                            'class'       => 'form-control',	
                                            'label'       => 'Telefono Casa:',	
                                            'placeholder' => '',
                                            'class_cont'  => 'col-md-4',
                                                                                            
                                        ])); ?>

                                        <?php echo e(Form::bsText('telefono_movil', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Telefono Movil:',
                                            'class_cont'  => 'col-md-4',
                                            'placeholder' => '',
                                                                                            
                                        ])); ?>

                                        <?php echo e(Form::bsText('telefono_oficina', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Telefono Oficina:',
                                            'class_cont'  => 'col-md-4',
                                            'placeholder' => '',
                                                                                            
                                        ])); ?>


                                        
                                    </div>

                                    <div class="tab-pane" id="tab4">

                                        <?php echo e(Form::bsPassword('password', '', [
                                            'class'       => 'form-control',
                                            'id' 		  => 'password',
                                            'label'       => 'Contraseña:',
                                            'class_cont'  => 'col-md-12',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>


                                        <?php echo e(Form::bsPassword('rpassword2', '', [
                                            'class'       => 'form-control',
                                            'id'		  => 'rpassword',
                                            'label'       => 'Confirma Contraseña:',
                                            'class_cont'  => 'col-md-12',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>

                    
                                        
                                        <div class="col-md-12"></div>
                                        
                                        
                                        <?php echo e(Form::bsText('Respuesta_1', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Repuesta:',
                                            'class_cont'  => 'col-md-8',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>

                                            <div class="col-md-12"></div>
                                       
                                        
                                        <?php echo e(Form::bsText('Respuesta_2', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Repuesta:',
                                            'class_cont'  => 'col-md-8',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>

                                            <div class="col-md-12"></div>
                                      
                                        
                                        <?php echo e(Form::bsText('Respuesta_3', '', [
                                            'class'       => 'form-control',
                                            'label'       => 'Repuesta:',
                                            'class_cont'  => 'col-md-8',
                                            'placeholder' => '',
                                            'required' => 'required'
                                                                                            
                                        ])); ?>


                                    </div>
                                </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                            <i class="fa fa-angle-left"></i> Atras </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next"> Siguiente
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                        <button type="" class="btn green button-submit" id="guardar"> Guardar <i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        
        <?php echo Form::close(); ?>





        </div>

    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make(isset($layouts) ? $layouts : 'base::layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>